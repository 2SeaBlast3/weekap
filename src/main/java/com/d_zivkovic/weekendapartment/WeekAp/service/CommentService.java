package com.d_zivkovic.weekendapartment.WeekAp.service;

import java.util.List;

import com.d_zivkovic.weekendapartment.WeekAp.model.Comment;

public interface CommentService {
	
	List<Comment> findAll();
	
	Comment findOne (Long id);
	
	Comment save (Comment comment);
	
	Comment delete(Long id);


}
