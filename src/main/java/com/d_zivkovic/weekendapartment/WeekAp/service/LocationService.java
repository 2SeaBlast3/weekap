package com.d_zivkovic.weekendapartment.WeekAp.service;

import java.util.List;

import com.d_zivkovic.weekendapartment.WeekAp.model.Location;

public interface LocationService {

	List<Location> findAll();
	
	Location findOne (Long id);
	
	Location save (Location location);
	
	Location delete(Long id);
}
