package com.d_zivkovic.weekendapartment.WeekAp.service;

import java.util.List;

import com.d_zivkovic.weekendapartment.WeekAp.model.Apartment;

public interface AppartmentService {
	
	List<Apartment> findAll();
	
	Apartment findOne (Long id);
	
	Apartment save (Apartment apartment);
	
	Apartment delete(Long id);

}
