package com.d_zivkovic.weekendapartment.WeekAp.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.d_zivkovic.weekendapartment.WeekAp.model.Address;
import com.d_zivkovic.weekendapartment.WeekAp.repository.AddressRepositoy;
import com.d_zivkovic.weekendapartment.WeekAp.service.AddresService;

@Service
public class AddressSeviceImpl implements  AddresService {
	
	AddressRepositoy addressRepositoy;
	@Override
	public List<Address> findAll() {
		return addressRepositoy.findAll();
	}

	@Override
	public Address findOne(Long id) {
		return addressRepositoy.findOne(id);
	}

	@Override
	public Address save(Address address) {
		
		return addressRepositoy.save(address);
	}

	@Override
	public Address delete(Long id) {
		Address address =addressRepositoy.findOne(id);
		
		addressRepositoy.delete(id);
		
		return address;
	}

}
