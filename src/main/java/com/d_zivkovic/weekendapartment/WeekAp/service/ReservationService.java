package com.d_zivkovic.weekendapartment.WeekAp.service;

import java.util.List;

import com.d_zivkovic.weekendapartment.WeekAp.model.Reservation;

public interface ReservationService {
	
	
	List<Reservation> findAll();
	
	Reservation findOne (Long id);
	
	Reservation save (Reservation reservation);
	
	Reservation delete(Long id);

}
