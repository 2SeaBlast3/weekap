package com.d_zivkovic.weekendapartment.WeekAp.service;

import java.util.List;

import com.d_zivkovic.weekendapartment.WeekAp.model.Address;

public interface AddresService {
	
	
	List<Address> findAll();
	
	Address findOne(Long id);
	
	Address save(Address address);
	
	Address delete(Long id);
	

}
