package com.d_zivkovic.weekendapartment.WeekAp.service;

import java.util.List;

import com.d_zivkovic.weekendapartment.WeekAp.model.Amenity;

public interface AmenityService {
	
	List<Amenity> findAll();
	
	Amenity findOne(Long id);
	
	Amenity save (Amenity amenity);
	
	Amenity delete (Long id);

}
