package com.d_zivkovic.weekendapartment.WeekAp.dto.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.d_zivkovic.weekendapartment.WeekAp.dto.LocationDTO;
import com.d_zivkovic.weekendapartment.WeekAp.model.Location;


@Component
public class LocationToDTO  implements Converter<Location, LocationDTO>{

	
	@Override
	public LocationDTO convert(Location source) {
		LocationDTO dto = new LocationDTO();
		
		dto.setId(source.getId());
		dto.setLatitude(source.getLatitude());
		dto.setLongitude(source.getLongitude());
		
		String temp = " " + source.getAddress().getStreet() +" " + source.getAddress().getStNumber()+
				" " + source.getAddress().getCity()+" " + source.getAddress().getPostNumb();
		dto.setAddress(temp);
		
		
		return dto;
	}
	
	public List<LocationDTO> convert(List<Location> source){
		List<LocationDTO> dto =new ArrayList<>();
		
		for (Location l: source) {
			dto.add(convert(l));
		}
		
		return dto;
	}

}
