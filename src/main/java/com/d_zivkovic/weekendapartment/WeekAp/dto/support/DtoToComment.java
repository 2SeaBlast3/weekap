package com.d_zivkovic.weekendapartment.WeekAp.dto.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.d_zivkovic.weekendapartment.WeekAp.dto.CommentDTO;
import com.d_zivkovic.weekendapartment.WeekAp.model.Comment;
import com.d_zivkovic.weekendapartment.WeekAp.service.AppartmentService;
import com.d_zivkovic.weekendapartment.WeekAp.service.CommentService;



@Component
public class DtoToComment implements Converter<CommentDTO, Comment>{
	
	@Autowired
	CommentService CommentService;
	
	@Autowired
	AppartmentService  appartmentService;

	@Override
	public Comment convert(CommentDTO source) {
		Comment ret = CommentService.findOne(source.getId());
		if (ret == null) {
			ret = new Comment();
		}
		ret.setId(source.getId());
		ret.setCommentText(source.getCommentText());
		ret.setRate(source.getRate());
//		ret.setGuestUser(source.);
		ret.setApartment(appartmentService.findOne(source.getApartmentId()));
		
		
		
		return ret;
	}
	
	private List<Comment> convert (List<CommentDTO> source){
		List<Comment> ret = new ArrayList<Comment>();
		for(CommentDTO dto: source) {
			ret.add(convert(dto));
		}
		return ret;
	}

}
