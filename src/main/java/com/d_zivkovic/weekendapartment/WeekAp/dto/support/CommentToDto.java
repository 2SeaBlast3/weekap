package com.d_zivkovic.weekendapartment.WeekAp.dto.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.d_zivkovic.weekendapartment.WeekAp.dto.CommentDTO;
import com.d_zivkovic.weekendapartment.WeekAp.model.Comment;

@Component
public class CommentToDto implements Converter<Comment, CommentDTO>{

	@Override
	public CommentDTO convert(Comment source) {
		CommentDTO dto = new CommentDTO();
		dto.setId(source.getId());
		dto.setCommentText(source.getCommentText());
		dto.setRate(source.getRate());
		dto.setGuestUserName(source.getGuestUser().getUserName());
		dto.setGuestUserId(source.getGuestUser().getId());
		dto.setApartmentId(source.getApartment().getId());
		return dto;
	}
	
	
	
	
	public List<CommentDTO> convert (List<Comment> source){
		
		List<CommentDTO> dto = new ArrayList<>();
		for (Comment c : source) {
			dto.add(convert(c));
		}
		
		return dto;
	}

}
