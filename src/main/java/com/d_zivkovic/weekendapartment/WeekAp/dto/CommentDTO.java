package com.d_zivkovic.weekendapartment.WeekAp.dto;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.d_zivkovic.weekendapartment.WeekAp.model.Apartment;
import com.d_zivkovic.weekendapartment.WeekAp.model.GuestUser;

public class CommentDTO {
	
	private Long id;
	
	private Long GuestUserId;
	
	private String guestUserName;
	
	private Long apartmentId;
	
	private String commentText;
	
	private Integer rate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGuestUserId() {
		return GuestUserId;
	}

	public void setGuestUserId(Long guestUserId) {
		GuestUserId = guestUserId;
	}

	public String getGuestUserName() {
		return guestUserName;
	}

	public void setGuestUserName(String guestUserName) {
		this.guestUserName = guestUserName;
	}

	public Long getApartmentId() {
		return apartmentId;
	}

	public void setApartmentId(Long apartmentId) {
		this.apartmentId = apartmentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}
	
	

}
