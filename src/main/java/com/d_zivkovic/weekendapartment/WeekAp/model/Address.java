package com.d_zivkovic.weekendapartment.WeekAp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Address {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String street;
	
	@Column
	private String stNumber;
	
	@Column
	private String city;
	@Column
	private Integer postNumb;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStNumber() {
		return stNumber;
	}
	public void setStNumber(String stNumber) {
		this.stNumber = stNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getPostNumb() {
		return postNumb;
	}
	public void setPostNumb(Integer postNumb) {
		this.postNumb = postNumb;
	}
	
	
	
	
	

}
