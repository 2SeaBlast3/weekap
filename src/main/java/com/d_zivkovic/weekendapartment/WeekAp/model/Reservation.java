package com.d_zivkovic.weekendapartment.WeekAp.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Reservation {
	@Id 
	@GeneratedValue
	private Long id;
	@ManyToOne @JoinColumn (name = "apartment")
	private Apartment apartment;
	
	private LocalDate firstDayOfReservation;
	private LocalDate LastDayOfReservation;
	
	private Integer lenghtOfRes;
	
	private Double totalPrice;
	
	@ManyToOne @JoinColumn ( name = "guestUser") 
	private GuestUser guestUser;
	
	private String status ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Apartment getApartment() {
		return apartment;
	}

	public void setApartment(Apartment apartment) {
		this.apartment = apartment;
	}

	public LocalDate getFirstDayOfReservation() {
		return firstDayOfReservation;
	}

	public void setFirstDayOfReservation(LocalDate firstDayOfReservation) {
		this.firstDayOfReservation = firstDayOfReservation;
	}

	public LocalDate getLastDayOfReservation() {
		return LastDayOfReservation;
	}

	public void setLastDayOfReservation(LocalDate lastDayOfReservation) {
		LastDayOfReservation = lastDayOfReservation;
	}

	public Integer getLenghtOfRes() {
		return lenghtOfRes;
	}

	public void setLenghtOfRes(Integer lenghtOfRes) {
		this.lenghtOfRes = lenghtOfRes;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public GuestUser getGuestUser() {
		return guestUser;
	}

	public void setGuestUser(GuestUser guestUser) {
		this.guestUser = guestUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	

}
