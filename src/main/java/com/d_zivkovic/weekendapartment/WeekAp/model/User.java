package com.d_zivkovic.weekendapartment.WeekAp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

@Entity
@Inheritance
public class User {

		@Id
		@GeneratedValue
		private Long id;
		@Column
		private String userName;
		@Column
		private String password;
		@Column
		private String frstName;
		@Column
		private String lastName;
		@Column
		private String gender;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getFrstName() {
			return frstName;
		}
		public void setFrstName(String frstName) {
			this.frstName = frstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		
		
		
}
