package com.d_zivkovic.weekendapartment.WeekAp.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Apartment {
	@Id 
	@GeneratedValue
	private Long id;
	
	private String type;
	
	private Integer numOfRooms;
	
	private Integer numOfGuests;
	
	@OneToOne
	private Location location;
	
	private LocalDate rentDate;
	
	@ManyToOne(fetch=FetchType.LAZY ) @JoinColumn ( name = "hostUser")
	private HostUser hostUser;
	
	private Double pricePerNight;
	
	private LocalTime checkIn;
	
	private LocalTime checkOut;
	
	@OneToMany(mappedBy = "apartment", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Comment> comments;
	
	
	private Boolean status;
	
	@OneToMany(mappedBy = "apartment", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Amenity> amenities;
	
	@OneToMany(mappedBy = "apartment", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Reservation> reservations;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(Integer numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	public Integer getNumOfGuests() {
		return numOfGuests;
	}

	public void setNumOfGuests(Integer numOfGuests) {
		this.numOfGuests = numOfGuests;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public LocalDate getRentDate() {
		return rentDate;
	}

	public void setRentDate(LocalDate rentDate) {
		this.rentDate = rentDate;
	}

	public HostUser getHostUser() {
		return hostUser;
	}

	public void setHostUser(HostUser hostUser) {
		this.hostUser = hostUser;
	}

	public Double getPricePerNight() {
		return pricePerNight;
	}

	public void setPricePerNight(Double pricePerNight) {
		this.pricePerNight = pricePerNight;
	}

	public LocalTime getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(LocalTime checkIn) {
		this.checkIn = checkIn;
	}

	public LocalTime getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(LocalTime checkOut) {
		this.checkOut = checkOut;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<Amenity> getAmenities() {
		return amenities;
	}

	public void setAmenities(List<Amenity> amenities) {
		this.amenities = amenities;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	
	
	
	
	
	

}
