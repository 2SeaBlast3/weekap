package com.d_zivkovic.weekendapartment.WeekAp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class GuestUser extends User {
	
	
//	private List<Apartment> rentApartments;
	
	@OneToMany(mappedBy = "guestUser", cascade = CascadeType.ALL,fetch = FetchType.LAZY)

	private List<Reservation> reservations;
	
	
	@OneToMany(mappedBy = "guestUser", cascade = CascadeType.ALL,fetch = FetchType.LAZY)

	private List<Comment> comments;


	public List<Reservation> getReservations() {
		return reservations;
	}


	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}


	public List<Comment> getComments() {
		return comments;
	}


	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	
	
}
