package com.d_zivkovic.weekendapartment.WeekAp.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class HostUser extends User{
	
	@OneToMany(mappedBy = "hostUser", fetch = FetchType.LAZY ,cascade = CascadeType.ALL) 
	private List<Apartment> apartmentsForRent;
	
	
	
	

	public List<Apartment> getApartmentsForRent() {
		return apartmentsForRent;
	}

	public void setApartmentsForRent(List<Apartment> apartmentsForRent) {
		this.apartmentsForRent = apartmentsForRent;
	}

	
	
}
