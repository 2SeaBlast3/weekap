package com.d_zivkovic.weekendapartment.WeekAp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.d_zivkovic.weekendapartment.WeekAp.model.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

}
