package com.d_zivkovic.weekendapartment.WeekAp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.d_zivkovic.weekendapartment.WeekAp.model.Address;

@Repository
public interface AddressRepositoy  extends JpaRepository<Address, Long>{

}
