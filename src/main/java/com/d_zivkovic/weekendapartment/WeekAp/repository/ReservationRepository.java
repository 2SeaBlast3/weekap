package com.d_zivkovic.weekendapartment.WeekAp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.d_zivkovic.weekendapartment.WeekAp.model.Reservation;

@Repository
public interface ReservationRepository  extends JpaRepository<Reservation, Long> {

}
