package com.d_zivkovic.weekendapartment.WeekAp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeekApApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeekApApplication.class, args);
	}

}
